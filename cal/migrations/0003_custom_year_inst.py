from django.db import migrations


def forwards_func(apps, *args):
    Year = apps.get_model('cal', 'Year')
    year_inst = [Year(name=year) for year in range(2018, 2101)]
    Year.objects.bulk_create(year_inst)

def reverse_func(apps, *args):
    Year = apps.get_model('cal', 'Year')
    Year.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('cal', '0002_custom_month_inst'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
