from django.db import migrations
from cal.constants import MONTHS_CHOICES


def forwards_func(apps, *args):
    Month = apps.get_model('cal', 'Month')
    months_inst = [Month(name=month[0]) for month in MONTHS_CHOICES]
    Month.objects.bulk_create(months_inst)

def reverse_func(apps, *args):
    Month = apps.get_model('cal', 'Month')
    Month.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('cal', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
