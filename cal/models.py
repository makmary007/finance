import calendar

from django.db import models
from django.utils.translation import gettext_lazy as _

from .constants import MONTHS_CHOICES
from .managers import MonthManager, YearManager


class Month(models.Model):
    name = models.IntegerField(verbose_name=_('name'), choices=MONTHS_CHOICES)

    objects = MonthManager()

    class Meta:
        verbose_name = _('month')
        verbose_name_plural = _('months')

    def __str__(self):
        return self.get_name_display()

    def last_day(self, year: int):
        cal = calendar.Calendar()
        last_day = [day for day in cal.itermonthdays(year, self.name) if day != 0][-1]
        return last_day

    def natural_key(self):
        return (self.name,)


class Year(models.Model):
    name = models.IntegerField(verbose_name=_('name'))

    objects = YearManager()

    class Meta:
        verbose_name = _('year')
        verbose_name_plural = _('years')

    def __str__(self):
        return str(self.name)

    def natural_key(self):
        return (self.name,)
