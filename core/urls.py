from django.urls import path, re_path
from rest_framework_simplejwt import views as jwt_views

from . import views

app_name = 'core'

urlpatterns = [
    path('register/', views.UserCreateView.as_view(), name='user_create'),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/'
            r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,32})/$',
            views.UserActivateView.as_view(), name='activate'),
    path('login/', jwt_views.TokenObtainPairView.as_view(), name='login'),
    path('restore_password/', views.RestorePasswordView.as_view(), name='restore_password'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', jwt_views.TokenVerifyView.as_view(), name='token_verify'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
]
