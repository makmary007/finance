import shutil

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import override_settings
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase

from core.factory import UserFactory
from core.utils import check_token, in_memory_test_image, user_hash

User = get_user_model()
fake = Faker()

class UserCreateTest(APITestCase):

    user_create = reverse('auth:user_create')

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.dummy.EmailBackend')
    @override_settings(task_always_eager=True)
    def test_register_create(self):
        email = fake.email()
        response = self.client.post(self.user_create, {'email': email})
        qs = User.objects.filter(email=email, is_active=False)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(qs.exists())
        for token in ['access', 'refresh']:
            self.assertTrue(check_token(response.data[token]))
        self.assertEqual(list(response.data.keys()), ['email', 'access', 'refresh'])

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.dummy.EmailBackend')
    @override_settings(task_always_eager=True)
    def test_register_dublicate(self):
        user = UserFactory(is_active=False)
        response = self.client.post(self.user_create, {'email': user.email})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserActivateTest(APITestCase):

    def setUp(self):
        self.user = UserFactory(is_active=False)
        self.u_hash = user_hash.make_token(self.user)
        self.uid = urlsafe_base64_encode(force_bytes(self.user.pk))
        self.url = reverse('auth:activate', kwargs={'uidb64': self.uid, 'token': self.u_hash})

    def test_activate(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, settings.LOGIN_URL)
        self.user.refresh_from_db()
        self.assertTrue(self.user.is_active)

    def test_user_not_found(self):
        self.user.delete()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_user_is_active(self):
        self.user.is_active = True
        self.user.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_incorrect_token(self):
        u_hash = '5ae-f75a363ba6c5023d3c05'
        url = reverse('auth:activate', kwargs={'uidb64': self.uid, 'token': u_hash})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ProfieViewTest(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.client.force_login(self.user)
        self.url = reverse('auth:profile')

    def tearDown(self):
        shutil.rmtree(settings.MEDIA_ROOT, ignore_errors=True)

    def test_update_user_name(self):
        name = fake.name()

        response = self.client.patch(self.url, {'name': name})
        self.user.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], self.user.name)

    @override_settings(DEFAULT_FILE_STORAGE='django.core.files.storage.FileSystemStorage')
    def test_update_user_avatar(self):
        avatar = in_memory_test_image()

        response = self.client.patch(self.url, {'avatar': avatar})
        self.user.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn(str(self.user.avatar), str(response.data['avatar']))

    @override_settings(DEFAULT_FILE_STORAGE='django.core.files.storage.FileSystemStorage')
    def test_invalid_avatar_height_width(self):
        avatar = in_memory_test_image(size=(400, 400))

        response = self.client.patch(self.url, {'avatar': avatar})
        self.user.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @override_settings(DEFAULT_FILE_STORAGE='django.core.files.storage.FileSystemStorage')
    def test_invalid_avatar_size(self):
        avatar = in_memory_test_image(size_byte=300000)

        response = self.client.patch(self.url, {'avatar': avatar})
        self.user.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
