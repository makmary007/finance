import base64
import random
import string
from io import BytesIO

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from PIL import Image
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import UntypedToken


def namegen(count):
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(count)])


class MakeHash(PasswordResetTokenGenerator):

    def _make_hash_value(self, user, timestamp):
        return user.email + str(user.pk) + user.password

user_hash = MakeHash()


def check_token(token):
    try:
        UntypedToken(token)
    except TokenError:
        return False
    return True


def in_memory_test_image(size=(50, 50), size_byte=0):
    """
    :param size: height and width of image
    :param size_byte: image size in bytes
    :return: <str> base64 of image
    """
    image = Image.new(mode='RGB', size=size)
    buffered = BytesIO(bytearray(size_byte))
    image.save(buffered, format="jpeg")
    image = base64.b64encode(buffered.getvalue())
    image = image.decode('utf-8')
    return image
