from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from finance import settings
from finance.celery import app as celery

User = get_user_model()


@celery.task
def send_email(email, subject, template, context=None):

    html_content = render_to_string(template_name=template, context=context)
    text_content = strip_tags(html_content)

    send_mail(
        subject=subject,
        message=text_content,
        recipient_list=[email],
        from_email=settings.DEFAULT_FROM_EMAIL,
        html_message=html_content,
    )
