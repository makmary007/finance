from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import gettext_lazy as _

from .models import CustomUser


@admin.register(CustomUser)
class UserAdmin(DjangoUserAdmin):

    list_display = ('email', 'avatar', 'name', 'is_superuser', 'is_active',)
    list_filter = ('is_superuser',)
    search_fields = ('email', 'name',)
    ordering = ('email', 'name',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('avatar', 'name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_superuser', 'is_staff',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'avatar', 'name')
        }),
    )
