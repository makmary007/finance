import factory
from django.contrib.auth import get_user_model
from factory.django import DjangoModelFactory

User = get_user_model()


class UserFactory(DjangoModelFactory):

    email = factory.Faker('email')
    is_active = True

    class Meta:
        model = User
