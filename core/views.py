from urllib.parse import urljoin

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from rest_framework import status
from rest_framework.exceptions import NotFound
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from core.utils import user_hash

from .serializers import (AuthSerializer, RestorePasswordSerializer,
                          UpdateProfileSerializer)
from .tasks import send_email
from .utils import namegen

User = get_user_model()


class UserCreateView(CreateAPIView):
    """
    This view creates user with is_active=False and generate password for it.
    Also creates access and refresh tokens for this user.
    Then send email with password for activation account.
    You can use it within 24 hours from the date of registration.
    """

    serializer_class = AuthSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        """
        This function creates a user with the necessary parameters.
        """

        # Use CreateAPIView class and rewrite create method couse to add
        # tokens to data. We can`t use APIView class couse add many methods(
        # get_serializer, get_success_headers and others)
        with transaction.atomic():
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            user = serializer.save(is_active=False)
            password = self.gen_password(user)

            refresh = RefreshToken.for_user(user)
            access = refresh.access_token
            refresh = str(refresh)
            access = str(access)
            headers = self.get_success_headers(serializer.data)
            mydata = serializer.data.copy()
            mydata.update({'access':access, 'refresh':refresh})

        project_url = request.build_absolute_uri('/')
        verify_link = self.verify_link(project_url, user)

        context = {'project_url': project_url,
                   'password': password,
                   'verify_link': verify_link,}

        template = 'core/email_register.html'

        subject = 'Verification email from FinanceX'
        send_email.delay(subject=subject, email=user.email, template=template, context=context)

        return Response(mydata, status=status.HTTP_201_CREATED, headers=headers)

    def gen_password(self, user):
        """
        This method creates password for user and save it.
        """
        password = namegen(10)
        user.set_password(password)
        user.save()
        return password

    def verify_link(self, project_url, user):
        """
        This method generate verify link:
        with uid 64 - encrypted user_id and
        hash - generated from user_id, password and email.
        Example: 'http://127.0.0.1:8000/auth/activate/NzA/5ag-b1280f5424444f9bbec2/'
        """
        u_hash = user_hash.make_token(user)
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        relative_url = reverse('auth:activate', kwargs={'uidb64': uid, 'token': u_hash})
        verify_link = urljoin(project_url, relative_url)
        return verify_link


class UserActivateView(APIView):
    """
    This view activate user, when user click to activation link in  the letter.
    User can use this link within 24 hours from the date of registration.
    """

    permission_classes = [AllowAny]

    def get(self, request, uidb64, token):
        user_id = urlsafe_base64_decode(uidb64)

        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise NotFound

        if user_hash.check_token(user, token):

            if not user.is_active:

                user.is_active = True
                user.save()

                return HttpResponseRedirect(settings.LOGIN_URL)

        raise NotFound


class RestorePasswordView(UserCreateView):
    """
    This view restore password to current email.
    New password send to user email.
    """

    permission_classes = [AllowAny]
    serializer_class = RestorePasswordSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data['email']

        try:
            user = User.objects.get(email=data)
        except User.DoesNotExist:
            return Response(status=status.HTTP_200_OK)

        password = self.gen_password(user)

        project_url = request.build_absolute_uri('/')

        context = {'project_url': project_url,
                   'password': password,
                   }

        template = 'core/email_restore_password.html'
        subject = 'Restore email from FinanceX'

        send_email.delay(subject=subject, template=template, context=context, email=user.email)

        return Response(status=status.HTTP_200_OK)


class ProfileView(UpdateAPIView):
    """
    This view update user`s name and avatar.
    """

    serializer_class = UpdateProfileSerializer

    def get_object(self):
        return self.request.user
