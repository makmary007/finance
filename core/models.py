import os

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _

from .constants import MAX_HEIGHT, MAX_SIZE, MAX_WIDTH
from .utils import namegen
from .validators import (ValidateProfileImageValidator,
                         ValidateSizeImageValidator)


class CustomUserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Use for custom create user model.

        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """
        Use for staffusers.

        """
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Use for superusers.

        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


def avatars_gen_name(instance, filename):
    ext = filename.split('.')[-1]
    filename = (f'{namegen(count=10)}{str(instance.id)}.{ext}')
    return os.path.join('avatars', filename)


class CustomUser(AbstractUser):
    i_error = _('Image can be not more')
    unit = _('Kb')
    h_error = _('height not more')
    w_error = _('width not more')
    result = format_lazy('{i_error} {MAX_SIZE} {unit}, {h_error} {MAX_HEIGHT} {px},\
                         {w_error} {MAX_WIDTH} {px}', i_error=i_error,
                         MAX_SIZE=MAX_SIZE//1024, unit=unit, h_error=h_error,
                         MAX_HEIGHT=MAX_HEIGHT, px='px', w_error=w_error, MAX_WIDTH=MAX_WIDTH)
    help_text = result

    email = models.EmailField(
        verbose_name=_('email address'),
        max_length=70,
        unique=True,
    )
    username = None
    avatar = models.ImageField(
        blank=True,
        null=True,
        verbose_name=_('avatar'),
        upload_to=avatars_gen_name,
        help_text=help_text,
        validators=[
            ValidateSizeImageValidator(size=MAX_SIZE),
            ValidateProfileImageValidator(height=MAX_HEIGHT, width=MAX_WIDTH)
        ]
    )
    name = models.CharField(max_length=100, blank=True, null=True, verbose_name=_('name'))

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('custom user')
        verbose_name_plural = _('custom users')
