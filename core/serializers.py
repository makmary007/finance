#pylint: disable=W0223
from django.contrib.auth import get_user_model
from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers

from .constants import MAX_HEIGHT, MAX_SIZE, MAX_WIDTH
from .validators import (ValidateProfileImageValidator,
                         ValidateSizeImageValidator)

User = get_user_model()


class AuthSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['email']


class UpdateProfileSerializer(serializers.ModelSerializer):

    avatar = Base64ImageField(required=False, validators=[
        ValidateSizeImageValidator(size=MAX_SIZE),
        ValidateProfileImageValidator(height=MAX_HEIGHT, width=MAX_WIDTH)
        ])

    class Meta:
        model = User
        fields = ['name', 'avatar']


class RestorePasswordSerializer(serializers.Serializer):

    email = serializers.EmailField(write_only=True)
