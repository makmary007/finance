from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.utils.deconstruct import deconstructible
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _


@deconstructible
class ValidateProfileImageValidator:

    def __init__(self, height, width):
        self.height = height
        self.width = width

    def __call__(self, value):
        if isinstance(value, ContentFile):
            width = value.image.width
            height = value.image.height
        else:
            width = value.width
            height = value.height

        if height > self.height or width > self.width:
            h_error = _('Max height of image is')
            w_error = _('max width of image is')
            result = format_lazy('{h_error} {height} {px}, {w_error} {width} {px}',
                                 h_error=h_error, height=self.height, px='px',
                                 w_error=w_error, width=self.width)
            raise ValidationError(result)

    def __eq__(self, other):
        return (
            isinstance(other, ValidateProfileImageValidator) and
            self.height == other.height and
            self.width == other.width
        )


@deconstructible
class ValidateSizeImageValidator:

    def __init__(self, size):
        self.size = size

    def __call__(self, value):

        if value.size > self.size:
            s_error = _('Max size of image is')
            unit = _('Kb')
            result = format_lazy('{s_error} {size} {unit}', s_error=s_error,
                                 size=self.size//1024, unit=unit)
            raise ValidationError(result)

    def __eq__(self, other):
        return (
            isinstance(other, ValidateSizeImageValidator) and
            self.size == other.size
        )
