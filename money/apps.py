from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MoneyConfig(AppConfig):
    name = 'money'
    verbose_name = _('money')

    # pylint: disable=import-outside-toplevel, unused-import
    def ready(self):
        from . import signals
