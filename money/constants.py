from django.utils.translation import gettext_lazy as _

CASH = 'CA'
DEBIT_CARD = 'DC'
CREDIT_CARD = 'CC'
CREDIT = 'CR'
BANK_ACCOUNT = 'BA'

ACCOUNT_TYPE = [
    (CASH, _('cash')),
    (DEBIT_CARD, _('debit card')),
    (CREDIT_CARD, _('credit card')),
    (CREDIT, _('credit')),
    (BANK_ACCOUNT, _('bank account')),
]


INCOMING = 'IN'
EXPENSE = 'EX'
TRANSFER = 'TR'

CATEGORY_TYPE = [
    (INCOMING, _('incoming')),
    (EXPENSE, _('expense')),
]

OPERATION_TYPE = [
    (INCOMING, _('incoming')),
    (EXPENSE, _('expense')),
    (TRANSFER, _('transfer')),
]
