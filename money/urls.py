from rest_framework import routers

from .views import AccountViewSet, CategoryViewSet, OperationViewSet

app_name = 'money'

router = routers.SimpleRouter()
router.register(r'accounts', AccountViewSet, basename='accounts')
router.register(r'categories', CategoryViewSet, basename='categories')
router.register(r'operations', OperationViewSet, basename='operations')

urlpatterns = router.urls
