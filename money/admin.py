from django.contrib import admin
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from .models import Account, Category, Operation, Transaction


class OperationAccountFilter(admin.SimpleListFilter):
    parameter_name = 'account'
    title = _('account')

    def lookups(self, request, model_admin):
        return Account.objects.all().values_list('id', 'name')

    def queryset(self, request, queryset):

        if self.value():
            return queryset.filter(Q(account=self.value()) | Q(target_account=self.value()))


class OperationInline(admin.TabularInline):
    model = Operation
    fields = ('summary', 'operation_type', 'date', 'target_account', 'category')
    fk_name = 'account'
    extra = 1


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    readonly_fields = ('created_date',)
    raw_id_fields = ('user',)
    list_display = ('name', 'balance', 'start_balance', 'account_type', 'is_default', 'user',)
    list_filter = ('is_default', 'account_type', 'user',)
    search_fields = ('name', 'user__email',)
    ordering = ('created_date',)
    readonly_fields = ('balance',)
    inlines = [OperationInline]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    list_filter = ('category_type', 'user',)
    list_display = ('name', 'category_type', 'user',)
    search_fields = ('name', 'user__email',)


class TransactionInline(admin.TabularInline):
    model = Transaction
    readonly_fields = ('account', 'summary', 'date', 'operation',)
    can_delete = False
    max_num = 1


@admin.register(Operation)
class OperationAdmin(admin.ModelAdmin):
    list_display = ('account', 'summary', 'operation_type', 'date', 'comment', 'target_account',
                    'category')
    raw_id_fields = ('account', 'target_account', 'category')
    list_filter = ('operation_type', OperationAccountFilter, 'date', 'category')
    search_fields = ('account__name', 'target_account__name', 'summary',)
    inlines = [
        TransactionInline,
    ]
