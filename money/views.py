from django.db.models import F, Sum
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .filters import CategoryFilter, OperationFilter
from .models import Account, Category, Operation
from .serializers import (AccountSerializer, CategorySerializer,
                          OperationSerializer)


class AccountViewSet(ModelViewSet):
    """
    This view CRUD accounts.
    If account is default or last we can`t delete it
    """
    serializer_class = AccountSerializer

    def get_queryset(self):
        return Account.objects.filter(user=self.request.user).order_by('pk')

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.is_default or self.get_queryset().count() == 1:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return super().destroy(request, *args, **kwargs)


class CategoryViewSet(ModelViewSet):
    """
    This view CRUD categories.
    We can`t delete last income and last expense category
    """
    serializer_class = CategorySerializer
    filterset_class = CategoryFilter
    filter_backends = [OrderingFilter, DjangoFilterBackend]
    ordering_fields = ['sum_op', '-sum_op']

    def get_queryset(self):
        qs = Category.objects.filter(user=self.request.user)
        return qs.annotate(sum_op=Sum(F('category_operations__summary')))

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        qs = self.get_queryset().filter(category_type=instance.category_type)

        if qs.count() == 1:
            content = {'error': 'last income or expense category'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

        return super().destroy(request, *args, **kwargs)


class OperationViewSet(ModelViewSet):
    """
    This view CRUD operations.
    """
    serializer_class = OperationSerializer
    filterset_class = OperationFilter
    filter_backends = [OrderingFilter, DjangoFilterBackend]
    ordering_fields = ['date', '-date']
    # for default ordering new date is first
    ordering = ['-date']

    def get_queryset(self):
        qs = Operation.objects.filter(account__user__email=self.request.user)
        return qs
