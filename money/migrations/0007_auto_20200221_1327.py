# Generated by Django 3.0.3 on 2020-02-21 10:27

from decimal import Decimal
from django.db import migrations
import djmoney.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('money', '0006_auto_20200211_1420'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='start_balance',
            field=djmoney.models.fields.MoneyField(decimal_places=2, default=Decimal('0'), default_currency='RUB', max_digits=14, verbose_name='start balance'),
        ),
        migrations.AddField(
            model_name='account',
            name='start_balance_currency',
            field=djmoney.models.fields.CurrencyField(choices=[('EUR', 'Euro'), ('RUB', 'Russian Ruble'), ('USD', 'US Dollar')], default='RUB', editable=False, max_length=3),
        ),
    ]
