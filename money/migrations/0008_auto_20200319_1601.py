# Generated by Django 3.0.4 on 2020-03-19 13:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('money', '0007_auto_20200221_1327'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='category',
        ),
        migrations.AddField(
            model_name='operation',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='category_operations', to='money.Category', verbose_name='operations of category'),
        ),
        migrations.AlterField(
            model_name='operation',
            name='target_account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='target_account_operation', to='money.Account', verbose_name='target account operation'),
        ),
    ]
