from django.db import models


# TODO: This manager doesn't used in logic anymore.
# But it is required by money.migrations.0002_auto_20191226_1323.py.
# Delete this manager after squashing migrations.
class CategoryManager(models.Manager):

    # https://docs.djangoproject.com/en/2.2/topics/serialization/#deserialization-of-natural-keys
    use_in_migrations = True

    def get_by_natural_key(self, user, name):
        return self.get(user=user, name=name)


class MoneyManager(models.Manager):

    # https://docs.djangoproject.com/en/2.2/topics/serialization/#deserialization-of-natural-keys
    use_in_migrations = True

    def get_by_natural_key(self, name, user_email):
        return self.get(user__email=user_email, name=name,)


class OperationManager(models.Manager):
    use_in_migrations = True

    def get_by_natural_key(self, date, account_name, user_email,):
        return self.get(date=date, account__name=account_name,
                        account__user__email=user_email,)


class TransactionManager(models.Manager):
    use_in_migrations = True

    def get_by_natural_key(self, date, account_name, user_email,
                           operation_date, op_account_name,
                           op_account_email,):
        return self.get(date=date, account__name=account_name,
                        account__user__email=user_email, operation__date=operation_date,
                        operation__account__name=op_account_name,
                        operation__account__user__email=op_account_email,)
