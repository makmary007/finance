from urllib.parse import urlencode

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils import timezone
from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from money.factories import (AccountFactory, CategoryFactory, OperationFactory,
                             OperationFactoryWithTransactions,
                             TransactionFactory, UserFactory)
from money.models import Account, Category, Transaction

from .constants import EXPENSE, INCOMING, TRANSFER

User = get_user_model()

fake = Faker()


class AccountModelTestCase(APITestCase):

    def setUp(self):
        self.first_account = AccountFactory(is_default=True)
        self.user = self.first_account.user
        self.new_name = 'new_name'
        self.first_account_url = reverse('money:accounts-detail', args=[self.first_account.pk])

    def test_dublicate_is_default(self):
        second_account = AccountFactory(is_default=True, user=self.user)
        third_account = AccountFactory(is_default=True)

        for inst in [self.first_account, second_account, third_account]:
            inst.refresh_from_db()

        self.assertFalse(self.first_account.is_default)
        self.assertTrue(second_account.is_default)
        self.assertTrue(third_account.is_default)

    def test_recalculate_balance(self):
        account = AccountFactory(start_balance=100)

        self.assertEqual(account.balance.amount, account.start_balance.amount)

        tr1 = TransactionFactory(account=account, operation__operation_type=EXPENSE)
        tr2 = TransactionFactory(account=account, operation__operation_type=INCOMING)
        expected_sum = sum((account.start_balance.amount, tr1.summary.amount, tr2.summary.amount))
        self.assertEqual(account.balance.amount, expected_sum)

        expected_result = (account.balance.amount - tr1.summary.amount - tr2.summary.amount)
        for inst in[tr1, tr2]:
            inst.delete()
        account.save()
        self.assertEqual(account.balance.amount, expected_result)

    def test_recalculate_balance_delete_account(self):
        account = AccountFactory(start_balance=10000)
        target_account = AccountFactory(start_balance=100)
        operation = OperationFactoryWithTransactions(account=account, operation_type=TRANSFER,
                                                     target_account=target_account)
        expected_result = (target_account.balance.amount - operation.summary.amount)
        account.delete()
        target_account.refresh_from_db()
        self.assertEqual(target_account.balance.amount, expected_result)

    def test_destroy_account(self):
        second_account = AccountFactory(user=self.user)
        third_account = AccountFactory(user=self.user)
        self.client.force_login(self.user)

        destroy_url = reverse('money:accounts-detail', args=[third_account.pk])
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        destroy_url = self.first_account_url
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.first_account.delete()
        destroy_url = reverse('money:accounts-detail', args=[second_account.pk])
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_account_equal_user_patch(self):
        self.client.force_login(self.user)

        update_url = self.first_account_url
        response = self.client.patch(update_url, {'name':self.new_name})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.patch(update_url, {'name':self.new_name})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_account_equal_user_get(self):
        self.client.force_login(self.user)
        get_url = self.first_account_url
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_account_equal_user_post(self):
        self.client.force_login(self.user)
        post_url = reverse('money:accounts-list')
        response = self.client.post(post_url, {'name':'test'})
        response_user = Account.objects.get(pk=response.data['id'])

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.user.pk, response_user.user.pk)


class OperationModelTestCase(APITestCase):

    def setUp(self):
        self.ex_operation = OperationFactory(operation_type=EXPENSE)
        self.user = self.ex_operation.account.user
        self.ex_category = CategoryFactory(category_type=EXPENSE, user=self.user)
        self.ex_operation_url = reverse('money:operations-detail', args=[self.ex_operation.pk])
        self.summary = fake.random_number(digits=4, fix_len=True)

    def test_target_account(self):
        account = AccountFactory()
        operation = OperationFactory.build(target_account=account)

        with self.assertRaises(ValidationError):
            operation.clean()

        operation = OperationFactory.build(operation_type=TRANSFER)

        with self.assertRaises(ValidationError):
            operation.clean()

        operation = OperationFactory.build(operation_type=TRANSFER,
                                           account=account, target_account=account)

        with self.assertRaises(ValidationError):
            operation.clean()

    def test_same_types(self):
        operation = OperationFactory.build(operation_type=INCOMING, category=self.ex_category)

        with self.assertRaises(ValidationError):
            operation.clean()

    def test_category_null(self):
        operation = OperationFactory.build(operation_type=TRANSFER, category=self.ex_category)

        with self.assertRaises(ValidationError):
            operation.clean()

    def test_category_not_null(self):
        operation = OperationFactory.build(operation_type=INCOMING, category=None)

        with self.assertRaises(ValidationError):
            operation.clean()

    # pylint: disable=invalid-unary-operand-type
    def test_transactions(self):
        operation = OperationFactory(operation_type=EXPENSE, category=self.ex_category)

        operation.make_transactions()
        qs = Transaction.objects.filter(operation=operation, operation__category=self.ex_category)
        self.assertEqual(qs.values()[0]['summary'], -operation.summary.amount)
        self.assertEqual(qs.count(), 1)

        operation2 = OperationFactory(operation_type=INCOMING)
        operation2.make_transactions()
        qs = Transaction.objects.filter(operation=operation2)
        self.assertEqual(qs.values()[0]['summary'], operation2.summary.amount)

        operation3 = OperationFactory(operation_type=TRANSFER,
                                      target_account=operation.account)
        operation3.make_transactions()
        qs = Transaction.objects.filter(
            operation=operation3, summary__in=(operation3.summary, -operation3.summary)
        )
        self.assertTrue(qs.count(), 2)

    def test_operation_equal_user_get(self):
        self.client.force_login(self.user)

        get_url = self.ex_operation_url
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_operation_equal_user_patch(self):
        self.client.force_login(self.user)

        update_url = self.ex_operation_url

        response = self.client.patch(update_url, {'summary':self.summary, 'summary_currency':'RUB'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.patch(update_url, {'summary':self.summary, 'summary_currency':'RUB'})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_operation_equal_user_post(self):
        self.client.force_login(self.user)

        post_url = reverse('money:operations-list')
        data = {
            'date': timezone.now(),
            'category':self.ex_category.pk,
            'summary':self.summary,
            'summary_currency':'RUB',
            'account':self.ex_operation.account.pk,
            }

        response = self.client.post(post_url, data)
        response_account = response.data['account']
        account = self.user.user_account.get(user=self.user).pk

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(account, response_account)

    def test_operation_equal_user_delete(self):
        self.client.force_login(self.user)

        destroy_url = self.ex_operation_url
        account = AccountFactory(user=self.user)
        operation = OperationFactory(account=account)
        destroy_url2 = reverse('money:operations-detail', args=[operation.pk])
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.delete(destroy_url2)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CategoryModelTestCase(APITestCase):

    def setUp(self):
        self.ex_category = CategoryFactory(category_type=EXPENSE)
        self.user = self.ex_category.user
        self.new_name = 'new_name'
        self.ex_category_url = reverse('money:categories-detail', args=[self.ex_category.pk])

    def test_delete_category(self):
        category = CategoryFactory(category_type=EXPENSE, user=self.user)

        self.client.force_login(self.user)

        destroy_url = reverse('money:categories-detail', args=[category.pk])
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        destroy_url = self.ex_category_url
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        in_category = CategoryFactory(category_type=INCOMING, user=self.user)
        destroy_url = reverse('money:categories-detail', args=[in_category.pk])
        response = self.client.delete(destroy_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_category_equal_user_patch(self):
        self.client.force_login(self.user)

        update_url = self.ex_category_url
        response = self.client.patch(update_url, {'name':self.new_name})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.patch(update_url, {'name':self.new_name})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_category_equal_user_get(self):
        self.client.force_login(self.user)

        get_url = self.ex_category_url
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_category_equal_user_post(self):
        self.client.force_login(self.user)

        post_url = reverse('money:categories-list')
        response = self.client.post(post_url, {'name':'test'})
        response_user = Category.objects.get(pk=response.data['id'])

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.user.pk, response_user.user.pk)

    def _create_operations_sorting(self, category, summary):
        OperationFactoryWithTransactions(category=category, summary=summary)

    def test_sorting(self):
        self.client.force_login(self.user)

        category_in_list = CategoryFactory.create_batch(3, user=self.user, category_type=INCOMING)
        category_ex_list = CategoryFactory.create_batch(2, user=self.user, category_type=EXPENSE)

        # create incomig operations
        self._create_operations_sorting(category=category_in_list[0], summary=5000)
        self._create_operations_sorting(category=category_in_list[1], summary=10000)
        self._create_operations_sorting(category=category_in_list[2], summary=300)

        # create expensive operations
        self._create_operations_sorting(category=category_ex_list[0], summary=5000)
        self._create_operations_sorting(category=category_ex_list[1], summary=10000)
        self._create_operations_sorting(category=self.ex_category, summary=300)

        base_url = "http://localhost:8000/money/categories/?"
        params_in = urlencode({'ordering': '-sum_op', 'category_type': INCOMING})
        url = f"{base_url}{params_in}"
        response = self.client.get(url)

        # most profitable sorting
        correct_ordering = [category_in_list[1].pk, category_in_list[0].pk, category_in_list[2].pk]
        current_ordering = [c['id'] for c in response.data['results']]
        self.assertEqual(current_ordering, correct_ordering)

        params_ex = urlencode({'ordering': '-sum_op', 'category_type': EXPENSE})
        url = f"{base_url}{params_ex}"
        response = self.client.get(url)

        # most expensive sorting
        correct_ordering = [category_ex_list[1].pk, category_ex_list[0].pk, self.ex_category.pk]
        current_ordering = [c['id'] for c in response.data['results']]
        self.assertEqual(current_ordering, correct_ordering)
