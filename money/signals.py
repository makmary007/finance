from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch import receiver

from money.models import Account, Operation

User = get_user_model()


@receiver(post_save, sender=Account)
def update_is_default(instance, sender, **kwargs):
    if instance.is_default:
        sender.objects.exclude(pk=instance.pk).filter(user=instance.user)\
                .filter(is_default=True).update(is_default=False)


@receiver(pre_save, sender=Account)
def recalculate_balance(instance, **kwargs):
    """
    Call calculate_balance and add it to instance
    """
    instance.balance = instance.calculate_balance()


@transaction.atomic
@receiver(post_save, sender=Operation)
def create_transactions(instance, **kwargs):
    """
    Make transactions
    """
    instance.make_transactions()


@receiver([post_save, post_delete], sender=Operation)
def recalculate_balance_by_transaction(instance, **kwargs):
    """
    Calculate balance with transactions, sender must be Operation,
    if sender Transaction, it does not call this signal when operation type TRANSFER
    """
    account = instance.account
    account.balance = account.calculate_balance()
    account.save()

    if instance.target_account:
        instance.target_account.save()
