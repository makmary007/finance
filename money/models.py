# pylint: disable=W0221
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import DecimalField, F, Sum
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator

from .constants import (ACCOUNT_TYPE, CATEGORY_TYPE, DEBIT_CARD, EXPENSE,
                        INCOMING, OPERATION_TYPE, TRANSFER)
from .managers import MoneyManager, OperationManager, TransactionManager

User = get_user_model()


class Account(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('name'))
    description = models.TextField(blank=True, null=True, verbose_name=_('description'))
    balance = MoneyField(max_digits=14, decimal_places=2, default_currency='RUB', default=0,
                         verbose_name=_('balance'))
    start_balance = MoneyField(max_digits=14, decimal_places=2, default_currency='RUB', default=0,
                               verbose_name=_('start balance'))
    account_type = models.CharField(max_length=2, choices=ACCOUNT_TYPE,
                                    verbose_name=_('account type'), default=DEBIT_CARD)
    created_date = models.DateTimeField(auto_now_add=True, verbose_name=_('created date'))
    is_default = models.BooleanField(default=False, verbose_name=_('is default'))
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_account',
                             verbose_name=_('user'))

    objects = MoneyManager()

    class Meta:
        unique_together = ('name', 'user',)
        verbose_name = _('account')
        verbose_name_plural = _('accounts')

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,) + self.user.natural_key()
    natural_key.dependencies = ['core.customuser']

    def calculate_balance(self):
        """
        Only return correct balance!
        """
        qs = self.account_transactions.select_related('account')
        if qs.exists():
            balance = qs.aggregate(
                summary=self.start_balance.amount + Sum(F('summary'), output_field=DecimalField())
            )
            balance = balance['summary']
        else:
            balance = self.start_balance

        return balance


class Category(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))
    category_type = models.CharField(max_length=2, choices=CATEGORY_TYPE,
                                     verbose_name=_('category type'), default=EXPENSE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_category',
                             verbose_name=_('user'))

    objects = MoneyManager()

    def get_category(self):
        return self.category_type

    class Meta:
        unique_together = ('name', 'user',)
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name,) + self.user.natural_key()
    natural_key.dependencies = ['core.customuser']


class Common(models.Model):
    summary = MoneyField(max_digits=14, decimal_places=2, default_currency='RUB',
                         verbose_name=_('summary'))
    date = models.DateTimeField(verbose_name=_('date'))

    class Meta:
        abstract = True


class Operation(Common):
    value = Decimal('0.01')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True,
                                 related_name='category_operations',
                                 verbose_name=_('operations of category'))
    summary = MoneyField(max_digits=14, decimal_places=2, default_currency='RUB',
                         verbose_name=_('summary'), validators=[MinMoneyValidator(value)])
    operation_type = models.CharField(max_length=2, choices=OPERATION_TYPE,
                                      verbose_name=_('operation type'), default=EXPENSE)
    comment = models.TextField(blank=True, null=True, verbose_name=_('comment'))
    account = models.ForeignKey(Account, on_delete=models.CASCADE,
                                related_name='account_operation',
                                verbose_name=_('account operation'))
    target_account = models.ForeignKey(Account, blank=True, null=True, on_delete=models.CASCADE,
                                       related_name='target_account_operation',
                                       verbose_name=_('target account operation'))

    objects = OperationManager()

    class Meta:
        unique_together = [['summary', 'date', 'account', 'operation_type']]
        verbose_name = _('operation')
        verbose_name_plural = _('operations')

    def __str__(self):
        # pylint: disable=invalid-name
        of = _('of')
        to = _('to')
        base_str = format_lazy('{operation_type} {summary} {of} {account}',
                               operation_type=self.get_operation_type_display(),
                               summary=self.summary, of=of, account=self.account)

        if self.category:
            of = _('of')
            to = _('to')
            category_str = format_lazy(' {of} {category}', of=of, category=self.category)
            return str(base_str) + str(category_str)

        if self.operation_type == TRANSFER:
            target_str = format_lazy(' {to} {target_account}', to=to,
                                     target_account=self.target_account)
            return str(base_str) + str(target_str)

        return str(base_str)

    def make_transactions(self):
        Transaction.objects.filter(operation=self).delete()

        transaction_dict = {'date':self.date, 'operation':self}

        if self.operation_type == TRANSFER:
            sum_expense = -(self.summary)
            sum_incoming = self.summary

            tr1 = Transaction(summary=sum_expense, account=self.account, **transaction_dict)
            tr2 = Transaction(summary=sum_incoming, account=self.target_account, **transaction_dict)

            Transaction.objects.bulk_create([tr1, tr2])

        else:

            if self.operation_type == EXPENSE:
                summary = -(self.summary)

            if self.operation_type == INCOMING:
                summary = self.summary

            Transaction.objects.create(summary=summary, account=self.account,
                                       **transaction_dict)

    def clean(self):
        if self.operation_type != TRANSFER and self.target_account:
            raise ValidationError(_('Target account must be null'))

        if self.operation_type == TRANSFER and not self.target_account:
            raise ValidationError(_('Please, specify target account'))

        if self.operation_type != TRANSFER and not self.category:
            raise ValidationError(_('Please, specify category'))

        if self.operation_type == TRANSFER and self.category:
            raise ValidationError(_('Category must be null when operation type is transfer'))

        if self.category and self.operation_type != self.category.category_type:
            raise ValidationError(_('Operation type and category type must be the same'))

        if self.account == self.target_account:
            raise ValidationError(_('Account and target account can not be the same'))

    def natural_key(self):
        return (self.date,) + self.account.natural_key()
    natural_key.dependencies = ['money.account']


class Transaction(Common):
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE,
                                  related_name='operation_transactions',
                                  verbose_name=_('transactions of operation'))
    account = models.ForeignKey(Account, on_delete=models.CASCADE,
                                related_name='account_transactions',
                                verbose_name=_('transactions of account'))

    objects = TransactionManager()

    class Meta:
        unique_together = [['summary', 'date', 'account', 'operation']]
        verbose_name = _('transaction')
        verbose_name_plural = _('transactions')

    def __str__(self):
        trans = _('Transaction')
        base_str = format_lazy('{tr} {operation}', tr=trans, operation=self.operation)
        return str(base_str)

    def natural_key(self):
        return (self.date,) + self.account.natural_key() + self.operation.natural_key()
    natural_key.dependencies = ['money.account', 'money.operation',]
