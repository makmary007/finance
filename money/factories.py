import factory
from django.utils import timezone
from factory.django import DjangoModelFactory, mute_signals
from factory.fuzzy import FuzzyChoice

from core.factory import UserFactory

from . import signals
from .constants import EXPENSE, INCOMING
from .models import Account, Category, Operation, Transaction


class AccountFactory(DjangoModelFactory):

    name = factory.Sequence(lambda n: f'Account {n:02d}')
    balance = factory.Faker('random_number', digits=4, fix_len=True)
    start_balance = balance
    is_default = False
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Account


class CategoryFactory(DjangoModelFactory):

    name = factory.Sequence(lambda n: f'Category {n:02d}')
    user = factory.SubFactory(UserFactory)
    category_type = FuzzyChoice([INCOMING, EXPENSE])

    class Meta:
        model = Category


@mute_signals(signals.post_save)
class OperationFactory(DjangoModelFactory):

    category = factory.SubFactory(CategoryFactory,
                                  category_type=factory.SelfAttribute('..operation_type'))
    summary = factory.Faker('random_number', digits=4, fix_len=True)
    date = timezone.now()
    operation_type = FuzzyChoice([INCOMING, EXPENSE])
    account = factory.SubFactory(AccountFactory)
    target_account = None

    class Meta:
        model = Operation


class OperationFactoryWithTransactions(DjangoModelFactory):

    category = factory.SubFactory(CategoryFactory,
                                  category_type=factory.SelfAttribute('..operation_type'))
    summary = factory.Faker('random_number', digits=4, fix_len=True)
    date = timezone.now()
    operation_type = FuzzyChoice([INCOMING, EXPENSE])
    account = factory.SubFactory(AccountFactory)
    target_account = None

    class Meta:
        model = Operation

    @factory.post_generation
    def make_related_transactions(obj, *args, **kwargs):
        obj.make_transactions()


@mute_signals(signals.post_save)
class TransactionFactory(DjangoModelFactory):

    summary = factory.LazyAttribute(
        lambda i: -i.operation.summary if i.operation.operation_type == EXPENSE
        else i.operation.summary
    )
    date = factory.SelfAttribute('operation.date')
    account = factory.SubFactory(AccountFactory, user=factory.SelfAttribute('..category.user'))
    operation = factory.SubFactory(
        OperationFactory,
        account=factory.SelfAttribute('..account'),
        operation_type=factory.SelfAttribute('..category.category_type')
    )

    class Meta:
        model = Transaction

    @factory.post_generation
    def recalculate_balance(obj, *args, **kwargs):
        account = obj.account
        account.balance = account.calculate_balance()
        account.save()
