from django.db.models import Q
from django_filters import rest_framework as filters

from .models import Category, Operation


class CategoryFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Category
        fields = ['category_type',]


class OperationFilter(filters.FilterSet):

    account = filters.CharFilter(method='account_concat_filter')
    date = filters.DateTimeFromToRangeFilter()

    # pylint: disable=unused-argument;
    @staticmethod
    def account_concat_filter(queryset, name, value):
        return queryset.filter(Q(account__name=value) | Q(target_account__name=value))

    class Meta:
        model = Operation
        fields = ['operation_type', 'category', 'summary', 'date']
