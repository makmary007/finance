[![pipeline status](https://gitlab.com/python-snakes/financex-backend/badges/master/pipeline.svg)](https://gitlab.com/python-snakes/financex-backend/commits/master)

API for FinanceX application.

# Requirements

* Docker
* Composer

# Installation

Build project using Docker

~~~
docker-compose build
~~~

After successfully built start project using follow command

~~~
docker-compose up
~~~

You can apply migrations using

~~~
docker-compose run -T --rm web python manage.py migrate
~~~

You can dump fixtures using

~~~
docker-compose run --no-deps -T web python manage.py dumpdata --natural-primary --natural-foreign --indent 4
 --exclude=auth --exclude=contenttypes --exclude=cal
 --exclude=sessions.session --exclude=admin.logentry > finance/fixtures/initial_data.json
~~~

You can load fixtures using

~~~
docker-compose run --no-deps -T web python manage.py loaddata finance/fixtures/initial_data.json
~~~

You must create ```.env``` file. It must be on your project root directory. 
You can see example in ```.env.example``` file with necessary settings on your project root directory.

# Using

You can use following urls for accessing project

* http://localhost:8000 - main page
* http://localhost:8000/admin - admin page

    login: test@test.local

    password: financetest1234

* http://localhost:8025 - MailHog interface for checking emails

You can access shell via

~~~
docker exec -ti financex-backend_web_1 sh
~~~

# Code syntax

We using PEP8 as code syntax rules. Max length of line is 100 symbols.
You can use `pylint` for checking style. For example:

~~~
docker-compose run --no-deps -T web find ./ -name "*.py" ! -path "./*/migrations/*" -exec pylint --exit-zero "{}" +
~~~

Additionally you can check and fix imports via

~~~
docker-compose run --no-deps -T web isort -rc
~~~

You can check migrations

~~~
docker-compose run -T web python manage.py makemigrations --check --dry-run
~~~
