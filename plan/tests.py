from random import choice

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from cal.models import Month, Year
from core.factory import UserFactory
from plan.factories import CategoryFactory

from .factories import ItemFactory, PlanFactory


class PlanModelTestCase(APITestCase):

    def setUp(self):
        self.plan = PlanFactory()
        self.plan_url = reverse('plan:plan_detail', args=[self.plan.month.name,
                                                          self.plan.year.name])
        self.user = self.plan.user
        self.client.force_login(self.user)

    def test_plan_equal_user_get(self):
        response = self.client.get(self.plan_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        month_name = choice(Month.objects.filter(month_plans__isnull=True).values_list('name',
                                                                                       flat=True))

        year_name = choice(Year.objects.filter(year_plans__isnull=True).values_list('name',
                                                                                    flat=True))

        get_url = reverse('plan:plan_detail', args=[month_name, year_name])
        response = self.client.get(get_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)

        response = self.client.get(self.plan_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_plan_post(self):
        post_url = reverse('plan:plan_list')

        month_name = choice(Month.objects.filter(month_plans__isnull=True).values_list('name',
                                                                                       flat=True))

        year_name = choice(Year.objects.filter(year_plans__isnull=True).values_list('name',
                                                                                    flat=True))

        data = {'month':month_name, 'year':year_name}
        response = self.client.post(post_url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_plan_equal_user_patch(self):
        month_name = choice(Month.objects.filter(month_plans__isnull=True).values_list('name',
                                                                                       flat=True))

        response = self.client.patch(self.plan_url, {'month':month_name})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(self.plan_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_plan_equal_user_delete(self):
        plan = PlanFactory()
        destroy_url2 = reverse('plan:plan_detail', args=[plan.month.name, plan.year.name])
        response = self.client.delete(self.plan_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.delete(destroy_url2)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ItemModelTestCase(APITestCase):

    def setUp(self):
        self.item = ItemFactory()
        category = CategoryFactory()
        self.month = self.item.plan.month.name
        self.year = self.item.plan.year.name
        self.item_url = reverse('plan:item_detail', args=[self.month, self.year, self.item.pk])
        self.category = self.item.category
        self.client.force_login(self.item.plan.user)
        self.data = {
            'amount':self.item.amount.amount,
            'amount_currency':'RUB',
            'category':category.pk,
            }

    def test_item_equal_user_get(self):
        response = self.client.get(self.item_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(self.item_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_item_post(self):
        post_url = reverse('plan:item_list', args=[self.month, self.year])

        response = self.client.post(post_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.post(post_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = response.data['non_field_errors'][0].split('=')[0]
        self.assertEqual('Item with this plan and category already exist', data)

    def test_item_plan_not_found(self):
        month_name = choice(Month.objects.filter(month_plans__isnull=True).values_list('name',
                                                                                       flat=True))
        year_name = choice(Year.objects.filter(year_plans__isnull=True).values_list('name',
                                                                                    flat=True))
        post_url = reverse('plan:item_list', args=[month_name, year_name])
        response = self.client.post(post_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_plan_equal_user_patch(self):
        item = ItemFactory.build()
        response = self.client.patch(self.item_url, {'amount':item.amount.amount,
                                                     'amount_currency':'RUB'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(self.item_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_plan_equal_user_put(self):
        item = ItemFactory.build()
        category = CategoryFactory()

        data = {
            'amount': item.amount.amount,
            'amount_currency': 'RUB',
            'category': category.pk,
            }

        response = self.client.put(self.item_url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.get(self.item_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_item_equal_user_delete(self):
        item = ItemFactory()
        destroy_url2 = reverse('plan:item_detail', args=[item.plan.month.name,
                                                         item.plan.year.name, item.pk])
        response = self.client.delete(self.item_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.logout()

        user = UserFactory()
        self.client.force_login(user)
        response = self.client.delete(destroy_url2)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
