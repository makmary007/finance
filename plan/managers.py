from django.db import models


class PlanManager(models.Manager):

    use_in_migrations = True

    def get_by_natural_key(self, month_name, year_name, user_email):
        return self.get(month__name=month_name, year__name=year_name, user__email=user_email,)
