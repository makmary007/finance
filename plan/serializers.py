from rest_framework import serializers
from rest_framework.exceptions import NotFound

from cal.models import Month, Year

from .models import Item, Plan


class PlanSerializer(serializers.ModelSerializer):
    month = serializers.SlugRelatedField(queryset=Month.objects.all(),
                                         slug_field='name', read_only=False)
    year = serializers.SlugRelatedField(queryset=Year.objects.all(),
                                        slug_field='name', read_only=False)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Plan
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = '__all__'
        read_only_fields = ('plan',)

    #pylint: disable=arguments-differ
    def validate(self, data):
        view = self.context.get('view')
        request = self.context.get('request')
        year_name = view.kwargs.get('year')
        month_name = view.kwargs.get('month')
        plan_qs = Plan.objects.filter(year__name=year_name, month__name=month_name,
                                      user=request.user)

        if not plan_qs.exists():
            raise NotFound

        plan = plan_qs.get()

        # For `patch` if not category
        category = data.get('category')

        if category:
            qs = view.get_queryset().filter(plan=plan, category=category)
            if qs.exists():
                raise serializers.ValidationError('Item with this plan and category already exist')
        return data
