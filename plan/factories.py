import factory
from factory.django import DjangoModelFactory

from cal.models import Month, Year
from core.factory import UserFactory
from money.factories import CategoryFactory

from .models import Item, Plan


class PlanFactory(DjangoModelFactory):
    month = factory.Iterator(Month.objects.all())
    year = factory.Iterator(Year.objects.all())
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Plan


class ItemFactory(DjangoModelFactory):
    amount = factory.Faker('random_number', digits=4, fix_len=True)
    category = factory.SubFactory(CategoryFactory)
    plan = factory.SubFactory(PlanFactory)

    class Meta:
        model = Item
