from django.contrib import admin

from .models import Item, Plan


class ItemInline(admin.TabularInline):
    model = Item


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    raw_id_fields = ('user',)
    list_display = ('month', 'year', 'user',)
    list_filter = ('month', 'user',)
    search_fields = ('user__email',)
    ordering = ('-year',)
    inlines = [ItemInline]
