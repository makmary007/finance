from django.shortcuts import get_object_or_404
from rest_framework.exceptions import NotFound
from rest_framework.viewsets import ModelViewSet

from .filters import ItemFilter
from .mixins import PlanMixin
from .models import Item, Plan
from .serializers import ItemSerializer, PlanSerializer


class PlanViewSet(ModelViewSet, PlanMixin):
    """
    This view CRUD plans.
    """
    serializer_class = PlanSerializer

    def get_queryset(self):
        return Plan.objects.filter(user=self.request.user).order_by('-year', '-month')

    def get_object(self):
        obj = get_object_or_404(self.return_plan_qs())
        return obj


class ItemViewSet(ModelViewSet, PlanMixin):
    """
    This view CRUD items.
    """
    serializer_class = ItemSerializer
    filterset_class = ItemFilter

    def get_queryset(self):

        if not self.return_plan_qs().exists():
            raise NotFound

        plan_id = self.return_plan_qs().values('pk')[0]['pk']
        qs = Item.objects.filter(plan_id=plan_id).order_by('-amount')
        return qs

    # Add plan to serializer
    def perform_create(self, serializer):
        plan = self.return_plan_qs().get()
        serializer.save(plan=plan)
