from decimal import Decimal

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _
from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator

from cal.models import Month, Year
from money.models import Category

from .managers import PlanManager

User = get_user_model()


class Plan(models.Model):
    month = models.ForeignKey(Month, on_delete=models.CASCADE, related_name='month_plans',
                              verbose_name=_('month'))
    year = models.ForeignKey(Year, on_delete=models.CASCADE, related_name='year_plans',
                             verbose_name=_('year'))
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_plans',
                             verbose_name=_('user'))

    objects = PlanManager()

    class Meta:
        unique_together = ('month', 'user', 'year')
        verbose_name = _('plan')
        verbose_name_plural = _('plans')

    def __str__(self):
        return f'{self.user} - {self.year} - {self.month}'

    def natural_key(self):
        return self.month.natural_key() + self.year.natural_key() + self.user.natural_key()
    natural_key.dependencies = ['core.customuser', 'cal.month', 'cal.year']


class Item(models.Model):
    value = Decimal('0.01')
    amount = MoneyField(max_digits=14, decimal_places=2, default_currency='RUB',
                        verbose_name=_('amount'), validators=[MinMoneyValidator(value)])
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='category_items',
                                 verbose_name=_('items of category'))
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE, related_name='plan_items',
                             verbose_name=_('items of plan'))

    class Meta:
        unique_together = ('plan', 'category',)
        verbose_name = _('item')
        verbose_name_plural = _('items')

    def __str__(self):
        return f'{self.amount} - {self.category}'

    def natural_key(self):
        return self.plan.natural_key() + self.category.natural_key()
    natural_key.dependencies = ['money.category', 'plan.plan']
