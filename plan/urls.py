from django.urls import path

from .views import ItemViewSet, PlanViewSet

app_name = 'plan'

plan_detail = PlanViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy',
})

plan_list = PlanViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

item_detail = ItemViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy',
})

item_list = ItemViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

urlpatterns = [
    path('plans/', plan_list, name='plan_list'),
    path('plans/<month>/<year>/', plan_detail, name='plan_detail'),
    path('plans/<month>/<year>/items/', item_list, name='item_list'),
    path('plans/<month>/<year>/items/<pk>/', item_detail, name='item_detail'),
]
