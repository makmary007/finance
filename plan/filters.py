from django_filters import rest_framework as filters

from .models import Item


class ItemFilter(filters.FilterSet):
    category_type = filters.CharFilter(field_name='category__category_type')

    class Meta:
        model = Item
        fields = ['category_type']
