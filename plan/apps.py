from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PlanConfig(AppConfig):
    name = 'plan'
    verbose_name = _('plan')
