from .models import Plan


class PlanMixin:

    def return_plan_qs(self):
        year_name = self.kwargs.get('year')
        month_name = self.kwargs.get('month')
        plan_qs = Plan.objects.filter(user=self.request.user)
        qs = plan_qs.filter(year__name=year_name, month__name=month_name)
        return qs
