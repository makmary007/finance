"""finance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView
from rest_framework.permissions import AllowAny
from rest_framework.schemas import get_schema_view

from core.permissions import superuser_required

urlpatterns = [
    path('admin/', admin.site.urls),
    path('openapi', get_schema_view(
        title='Finance X Project',
        description='API for our project',
        permission_classes=[AllowAny]
    ), name='openapi-schema'),
    path('', superuser_required(TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'openapi-schema'}
    )), name='swagger'),
    path('auth/', include('core.urls', namespace='auth')),
    path('money/', include('money.urls', namespace='money')),
    path('budget/', include('plan.urls', namespace='budget')),
]
