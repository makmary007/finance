FROM python:3.8-alpine
ENV PYTHONUNBUFFERED 1
RUN apk update && \
    apk add --virtual .build-deps build-base && \
    apk add gettext postgresql-dev postgresql-client python3-dev jpeg-dev zlib-dev tzdata

WORKDIR /code
COPY requirements.txt /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN apk del .build-deps
COPY . /code/
